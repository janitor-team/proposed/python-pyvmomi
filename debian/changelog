python-pyvmomi (6.7.1-4) UNRELEASED; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + python-pyvmomi-doc: Add Multi-Arch: foreign.

 -- Debian Janitor <janitor@jelmer.uk>  Mon, 20 Apr 2020 21:32:42 +0000

python-pyvmomi (6.7.1-3) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Wed, 31 Jul 2019 14:31:12 +0200

python-pyvmomi (6.7.1-2) unstable; urgency=medium

  * Upload to unstable

 -- Mathieu Parent <sathieu@debian.org>  Sat, 17 Nov 2018 17:33:30 +0100

python-pyvmomi (6.7.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Add trailing tilde to min version depend to allow
    backports
  * d/control: Use team+openstack@tracker.debian.org as maintainer
  * d/control: Fix wrong Vcs-*

  [ Mathieu Parent ]
  * Upload to experimental
  * New upstream version (Closes: #860285)
    - Removing data_files.patch, merged upstream
    - Add patch to unpin vcrpy out of =1.12.0 and remove SSL tunnel tests
  * Package takeover:
    - Maintainer: Debian OpenStack -> Debian Python Modules Team
    - Uploaders: Remove Julien Danjou, Thomas Goirand, Mehdi Abaakouk and Joshua
      Kwan
    - Uploaders: Add myself
  * Update d/watch to github as doc is missing on pypi
  * Standards-Version: 4.2.1
  * Enable python3 tests and required Build-Depends
  * Debhelper compat: 9 -> 11
  * Add NOTICE.txt in python{,3}-pyvmomi.docs

 -- Mathieu Parent <sathieu@debian.org>  Fri, 16 Nov 2018 06:22:46 +0100

python-pyvmomi (5.5.0-2014.1.1-4) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * Install examples (Closes: #855414)
  * d/copyright: Reorder
  * Updating standards version to 4.1.1.

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.

 -- Ondřej Nový <onovy@debian.org>  Tue, 31 Oct 2017 15:55:20 +0100

python-pyvmomi (5.5.0-2014.1.1-3) unstable; urgency=medium

  * Now suggests correctly ython-pyvmomi-doc and not pyvmomi-doc. Thanks
    to Paul Wise <pabs@debian.org> for reporting (Closes: #772450).

 -- Thomas Goirand <zigo@debian.org>  Wed, 07 Jan 2015 15:02:17 +0000

python-pyvmomi (5.5.0-2014.1.1-2) unstable; urgency=medium

  * Added missing dh-python build-depends.
  * Ran wrap-and-sort.
  * Standards-Version is now 3.9.6 (no change).

 -- Thomas Goirand <zigo@debian.org>  Mon, 20 Oct 2014 23:23:52 +0800

python-pyvmomi (5.5.0-2014.1.1-1) unstable; urgency=low

  * New upstream version.
  * Add RST documentation in python-pyvmomi-doc.
  * Add Python3 support in python3-pyvmomi.
  * Add upstream maintainer information.
  * Add new watch file using PyPI.
  * Use pybuild, which automates the testing and install phases.

 -- Joshua Kwan <joshk@triplehelix.org>  Wed, 03 Sep 2014 12:54:43 -0400

python-pyvmomi (5.5.0-1) unstable; urgency=low

  * Initial release. (Closes: #738325)

 -- Thomas Goirand <zigo@debian.org>  Sun, 09 Feb 2014 14:56:33 +0800
